package com.rezayusrilnaufal_10191071.persistence;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText Masukkan;
    private Button Eksekusi;
    private TextView Hasil;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Masukkan = findViewById(R.id.input);
        Eksekusi = findViewById(R.id.save);
        Hasil = findViewById(R.id.output);

        preferences = getSharedPreferences("persistence", MODE_PRIVATE);
        Eksekusi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view){
                getData();
                Toast.makeText(getApplicationContext(), "Data Tersimpan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(){
        String getKonten = Masukkan.getText().toString();
        editor= preferences.edit();
        editor.putString("data_saya", getKonten);
        editor.apply();
        Hasil.setText("Output Data: "+preferences.getString("data_saya", null));
    }
}